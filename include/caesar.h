/**************************************************************************
 *   caesar.h  --  This file is part of Crypted.                          *
 *                                                                        *
 *   Copyright (C) 2019 batuesh                                           *
 *                                                                        *
 *   Crypted is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   Crypted is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#ifndef _CAESAR_H
#define _CAESAR_H

#ifndef _GLIBCXX_IOSTREAM
#include <iostream>
#endif
#ifndef _GLIBCXX_LOCALE
#include <locale>
#endif

class caesar
{
    public:
        caesar(std::basic_string<char> &input, int &shift);
        void encrypt();
        void decrypt();
        std::basic_string<char> get_input() const;
    private:
        std::basic_string<char> input = "", temp = "";
        int shift_count = 0;
};

#endif //CAESAR_H
