/**************************************************************************
 *   keyword.h  --  This file is part of Crypted.                         *
 *                                                                        *
 *   Copyright (C) 2019 batuesh                                           *
 *                                                                        *
 *   Crypted is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   Crypted is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#ifndef _KEYWORD_H
#define _KEYWORD_H

#ifndef _GLIBCXX_IOSTREAM
#include <iostream>
#endif
#ifndef _GLIBCXX_ARRAY
#include <array>
#endif
#ifndef _GLIBCXX_CSTDLIB
#include <cstdlib>
#endif
#ifndef _GLIBCXX_STRING
#include <string>
#endif
#ifndef _GLIBCXX_LOCALE
#include <locale>
#endif

#define ALPHABET 26

class keyword
{
    public:
        keyword(std::basic_string<char> &input, std::basic_string<char> &key);
        void encrypt();
        void decrypt();
        void abcrypt();
        void abcplain();
        void strip_double_char(std::basic_string<char> &input);
        std::basic_string<char> get_input() const;
    private:
        int code = 0, base_upper_code = 65, base_lower_code = 97;
        std::basic_string<char> input = "", temp = "", key = "";
        std::basic_string<char> upper_encrypting = "", lower_encrypting = "";
        std::basic_string<char> upper_plain = "", lower_plain = "";
};

#endif
