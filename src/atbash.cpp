/**************************************************************************
 *   atbash.cpp  --  This file is part of Crypted.                        *
 *                                                                        *
 *   Copyright (C) 2019 batuesh                                           *
 *                                                                        *
 *   Crypted is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   Crypted is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

#ifndef _ATBASH_H
#include "atbash.h"
#endif

atbash::atbash(std::basic_string<char> &input)
{
    this->input = input;
    this->temp = input;
}

void atbash::encrypt()
{
    std::basic_string<char> &input = this->input;
    std::basic_string<char> &temp = this->temp;
    int &code = this->code;

    for (size_t num = 0;num < input.size();++num)
    {
        if ( islower(input[num]) )
            code = 97;
        else if ( isupper(input[num]) )
            code = 65;
        else
            continue;
        input[num] = (temp[num] + ALPHABET - (temp[num] - code) - (temp[num] - code) - 1);
    }
}

void atbash::decrypt()
{
    encrypt();
    encrypt();
}

std::basic_string<char> atbash::get_input() const
{
    return this->input;
}
